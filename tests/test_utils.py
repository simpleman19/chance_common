import unittest
from chance_common.common.utils import ToDictMixin, FromDictMixin, guid


class TestToDictMixin(unittest.TestCase):
    def test_empty_obj(self):
        class TestClass(ToDictMixin):
            pass

        obj = TestClass()
        self.assertEqual(obj.to_dict(), {})

    def test_example_obj(self):
        class TestClass(ToDictMixin):
            hello = "World"

        obj = TestClass()
        self.assertEqual(obj.to_dict(), {'hello': 'World'})

    def test_excluded_obj(self):
        class TestClass(ToDictMixin):
            hello = "World"
            excluded = "Error"
            excluded_fields = ['excluded']

        obj = TestClass()
        self.assertEqual(obj.to_dict(), {'hello': 'World'})

    def test_init_fields_obj(self):
        class TestClass(ToDictMixin):
            def __init__(self):
                self.hello = "World"

        obj = TestClass()
        self.assertEqual(obj.to_dict(), {'hello': 'World'})

    def test_multi_inheritance(self):
        class TestClass(ToDictMixin):
            hello: str = "World"

        class TestClass2(TestClass):
            foo: str = "bar"

        obj = TestClass2()
        self.assertEqual(obj.to_dict(), {'hello': 'World', 'foo': 'bar'})


class TestFromDictMixin(unittest.TestCase):
    def test_empty_dict(self):
        class TestClass(FromDictMixin):
            hello: str = None

        obj = TestClass.from_dict({})
        self.assertIsNone(obj.hello)

    def test_single_val_dict(self):
        class TestClass(FromDictMixin):
            hello: str

        obj = TestClass.from_dict({'hello': 'World'})
        self.assertEqual(obj.hello, 'World')
        self.assertIsInstance(obj, TestClass)

    def test_multi_val_dict(self):
        class TestClass(FromDictMixin):
            hello: str
            foo: str

        obj = TestClass.from_dict({'hello': 'World', 'foo': 'bar'})
        self.assertEqual(obj.hello, 'World')
        self.assertEqual(obj.foo, 'bar')
        self.assertIsInstance(obj, TestClass)

    def test_multi_inheritance(self):
        class TestClass(FromDictMixin):
            hello: str

        class TestClass2(TestClass):
            foo: str

        obj = TestClass2.from_dict({'hello': 'World', 'foo': 'bar'})
        self.assertEqual(obj.hello, 'World')
        self.assertEqual(obj.foo, 'bar')
        self.assertIsInstance(obj, TestClass)


class TestToAndFromDictMixin(unittest.TestCase):

    def test_to_and_from(self):
        class TestClass(ToDictMixin, FromDictMixin):
            hello: str
            foo: str

        obj1 = TestClass()
        obj1.hello = "World"
        obj1.foo = "bar"
        obj1_dict = obj1.to_dict()
        obj2 = TestClass.from_dict(obj1_dict)
        self.assertEqual(obj1.hello, obj2.hello)
        self.assertEqual(obj1.foo, obj2.foo)
        self.assertIsInstance(obj2, TestClass)


class TestGuid(unittest.TestCase):

    def test_guid(self):
        str_guid = guid()
        self.assertEqual(len(str_guid), 32)

    def test_guid_changes(self):
        str_guid = guid()
        str_guid2 = guid()
        self.assertNotEqual(str_guid, str_guid2)

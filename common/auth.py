from flask import g, request, current_app as app, jsonify, url_for, redirect, make_response
from chance_common.common.http_auth import HTTPTokenAuth, MultiAuth, HTTPBasicAuth
from chance_common.common.custom_logging import get_logger
import jwt
from datetime import timedelta, datetime
from chance_common.common.db.sqlalchemy.models.auth import PasswordEntry
from chance_common.common.db.sqlalchemy.models.user import User
from chance_common.common.db.sqlalchemy.models import db

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth('Bearer')
token_optional_auth = HTTPTokenAuth('Bearer')
multi_auth = MultiAuth(basic_auth, token_auth)

logger = get_logger(__name__)


@basic_auth.verify_password
def verify_password(email, password):
    if not email or not password:
        email, password = load_email_and_pass_from_form(request)
    if not email or not password:
        logger.warn("Failed to find email and password....")
        return False
    logger.info("Verifying password for email: %s", email)
    user_account, password_entry = load_user_and_pass(email)
    if not user_account or not password_entry or not password_entry.verify_password(password):
        logger.info("Invalid password for email: %s", email)
        g.current_user_guid = None
        return False
    g.current_user_guid = user_account.guid
    logger.info("Authenticated email: %s guid: %s using basic auth", email, password_entry.guid)
    return True


def load_user_and_pass(email):
    user_account: User = User.query.filter_by(email=email).one_or_none()
    if user_account:
        password_entry = PasswordEntry.query.get(user_account.current_password_id)
    else:
        password_entry = None
    return user_account, password_entry


def load_email_and_pass_from_form(r):
    form = r.form
    if form:
        return form.get('email', None), form.get('password', None)
    else:
        # Hiding password (please never log a password...)
        printed_form = form.copy()
        if "password" in printed_form:
            printed_form['password'] = "Hidden"
        logger.debug("Could not find email in form: %s", printed_form)
        return None, None


@basic_auth.error_handler
def password_error():
    """Return a 401 error to the client."""
    resp = make_response(redirect(url_for('main.index')))
    resp.headers.add_header('WWW-Authenticate', 'Bearer realm="Authentication Required"')
    return resp


@token_auth.verify_token
def verify_token(token):
    logger.info("Verifying token")
    try:
        decoded = decode_token(token, catch_exceptions=False)
        logger.info("Token successfully decoded for user: %s", decoded.get("user_guid", None))
    except jwt.ExpiredSignatureError:
        logger.warn('Expired Token')
        return False
    except jwt.DecodeError:
        logger.error('Decode Token Error')
        return False
    guid = decoded.get('user_guid', None)
    if not guid:
        return False
    exists = db.session.query(User.id).filter_by(guid=guid).scalar() is not None
    if not exists:
        logger.warn('Got valid token but for an invalid user: %s', guid)
        return False
    g.token = decoded
    g.current_user_guid = guid
    return True


@token_auth.error_handler
def token_error():
    """Return a 401 error to the client."""
    resp = make_response(redirect(url_for('auth.login')))
    resp.headers.add_header('WWW-Authenticate', 'Bearer realm="Authentication Required"')
    return resp


@token_optional_auth.verify_token
def verify_optional_token(token):
    if not token:
        g.token = None
        g.current_user_guid = None
        return True
    return verify_token(token)


@token_optional_auth.error_handler
def multi_error():
    """Return a 401 error to the client."""
    resp = make_response(redirect(url_for('main.index')))
    resp.headers.add_header('WWW-Authenticate', 'Bearer realm="Authentication Required"')
    return resp


def create_token(user_id=None, **kwargs):
    if user_id is not None:
        if get_current_token():
            token_dict = get_current_token()
            for key, value in kwargs.items():
                token_dict[key] = value
            token_dict['exp'] = (datetime.utcnow() + timedelta(hours=app.config['TOKEN_EXPIRE_MINUTES']))
            token = jwt.encode(token_dict, app.config['JWT_SECRET_KEY'], algorithm='HS256')
        else:
            token = jwt.encode({'user_guid': str(user_id),
                                'exp': (datetime.utcnow() + timedelta(hours=app.config['TOKEN_EXPIRE_MINUTES']))
                                }, app.config['JWT_SECRET_KEY'], algorithm='HS256')
        return token
    else:
        return ""


def get_current_token():
    if request.cookies.get('Bearer'):
        token = decode_token(request.cookies.get('Bearer'), catch_exceptions=True)
    else:
        token = None
    return token


def decode_token(token, catch_exceptions=True, verify=True):
    if catch_exceptions:
        try:
            decoded = jwt.decode(token, app.config['JWT_SECRET_KEY'], algorithms=['HS256'], verify=verify)
        except jwt.ExpiredSignatureError:
            logger.warn('Decoded an expired Token')
            return None
        except jwt.DecodeError:
            logger.warn('Tried to decode an invalid token')
            return None
    else:
        decoded = jwt.decode(token, app.config['JWT_SECRET_KEY'], algorithms=['HS256'], verify=verify)
    return decoded


def get_token(resp=None, remember_me=False):
    guid = g.current_user_guid
    logger.info("Refreshing token for user: %s", guid)
    if guid is None:
        logger.warn("Token request for user that is not logged in")
        return password_error()
    logger.debug("Getting token for: %s", guid)
    token = create_token(guid)
    if not resp:
        logger.debug("Did not include a response, creating basic json response with token for user: %s", guid)
        resp = jsonify({'token': token.decode('UTF-8')})
    expire_minutes = app.config['TOKEN_EXPIRE_MINUTES']
    if remember_me:
        logger.debug("User requested remember me, extending token for user: %s", guid)
        expire_minutes = app.config['TOKEN_EXPIRE_MINUTES_REMEMBER_ME']
    resp.set_cookie('Bearer',
                    token,
                    expires=datetime.utcnow() + timedelta(minutes=expire_minutes))
    logger.debug("Successfully refreshed token for user: %s", guid)
    return resp


def decode_token_no_verification(token, catch_exceptions=True):
    if catch_exceptions:
        try:
            decoded = jwt.decode(token, algorithms=['HS256'], verify=False)
            return decoded
        except jwt.ExpiredSignatureError:
            logger.warn('Decoded an expired Token')
            return None
        except jwt.DecodeError:
            logger.warn('Tried to decode an invalid token')
            return None
    else:
        decoded = jwt.decode(token, algorithms=['HS256'], verify=False)
        return decoded

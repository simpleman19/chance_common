import logging
import sys

formatter = logging.Formatter('(%(asctime)s):%(levelname)s:%(name)s:%(funcName)s--%(message)s')


def setup_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Kill default handler to allow for root logger to be the only handler that catches
    log = logging.getLogger('werkzeug')
    if log:
        log.handlers = []


def redirect_to_file(filepath, mode='a'):
    logger = logging.getLogger()
    logger.handlers = []
    handler = logging.FileHandler(filepath, mode=mode)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def get_logger(name, log_level=logging.DEBUG):
    app_logger = logging.getLogger(name)
    app_logger.setLevel(log_level)
    return app_logger


# Auto configure root logger when imported
setup_logger()

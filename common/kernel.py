from typing import List, Optional

from flask import Flask, Blueprint
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import jinja2

from chance_common.common.config import Config
from chance_common.common.custom_logging import get_logger
from chance_common.common.dynamic_import.blueprint import scan_blueprints
from chance_common.common.dynamic_import.models import scan_models

logger = get_logger(__name__)


def build_app(import_name, configuration: Config):
    root_path = configuration.ROOT_PATH
    logger.debug(f"App root path: {root_path}")

    app = Flask(import_name, static_url_path='', root_path=root_path)
    app.config.from_object(configuration)
    app.host = '0.0.0.0'
    return app


def bootstrap_kernel(import_name, configuration: Config, db: Optional[SQLAlchemy] = None,
                     migrate: Optional[Migrate] = None):
    logger.info(f"Booting app kernel for app: {import_name}")
    root_path = configuration.ROOT_PATH

    app = build_app(import_name, configuration)

    # Initialize flask extensions
    logger.debug("Initializing extensions...")
    if db:
        db.init_app(app)
        migrate.init_app(app=app, db=db)

    logger.debug("Loading models...")
    scan_models(root_path, configuration.MODEL_DIRS, configuration.MODEL_EXCLUDE_FILES)

    logger.info("Registering blueprints...")
    blueprints: List[Blueprint] = scan_blueprints(root_path)

    # Registering custom endpoint handlers
    from chance_common.common.flask_decorators import register_endpoints
    register_endpoints()

    # Register web application routes
    for b in blueprints:
        prefix = b.url_prefix
        if not prefix:
            prefix = '/'
        logger.debug(f"Registering blueprint: {b.name} at prefix: {prefix}")
        app.register_blueprint(b)

    logger.info("Registering formatters...")
    from chance_common.common.formatters import register_formatters
    register_formatters(app)

    logger.info("Registering context processors...")
    from chance_common.common.context_processors import register_context_processors
    register_context_processors(app)

    logger.info("Registering before request funcs...")
    from chance_common.common.loaders import load_account
    app.before_request_funcs.setdefault(None, []).append(load_account)

    logger.info("Loading in custom jinja2 template loaders...")
    template_dirs = configuration.TEMPLATE_DIRS

    jinja_loader = jinja2.ChoiceLoader([
        jinja2.FileSystemLoader(template_dirs)
    ])
    app.jinja_loader = jinja_loader

    logger.info("Finished booting kernel")
    return app

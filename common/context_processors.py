from flask import g
from chance_common.common.auth import token_optional_auth
from chance_common.common.loaders import load_account
from chance_common.common.custom_logging import get_logger

logger = get_logger(__name__)

processors = []


def register_context_processors(app):
    for p in processors:
        logger.debug("Registering context processor: %s.%s", p.__module__, p.__name__)
        app.template_context_processors[None].append(p)


def context_processor(f):
    logger.debug("Adding decorated context processor function: %s.%s", f.__module__, f.__name__)
    processors.append(f)
    return f


@context_processor
def account_injection_processor():
    logger.debug('Injecting account into g')
    if not g.get('current_user_guid', None):
        logger.debug('Current user guid not loaded')
        auth = token_optional_auth.get_auth()
        token_optional_auth.authenticate(auth)

    load_account()

    if g.get('account'):
        logger.debug('Successfully loaded account')
        return dict(account=g.account)
    logger.debug('Failed to load account')
    return dict()

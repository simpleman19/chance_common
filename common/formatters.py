import urllib.parse

from dateutil import parser
from markupsafe import Markup

from chance_common.common.custom_logging import get_logger

formatters = {}
logger = get_logger(__name__)


def formatter(formatter_name):
    def wrapper(f):
        logger.debug("Adding decorated formatter: %s as function %s.%s", formatter_name, f.__module__, f.__name__)
        formatters[formatter_name] = f
        return f

    return wrapper


@formatter('formatdatetime')
def format_datetime(value, date_format="%m/%d/%Y %I:%M %p"):
    """Format a date time to (Default): d Mon YYYY HH:MM P"""
    if value is None:
        return ""
    if isinstance(value, str):
        value = parser.parse(value)
    return value.strftime(date_format)


@formatter('formatdate')
def format_date(value, format_str="%m/%d/%Y"):
    """Format a date time to (Default): 00/00/0000"""
    if value is None:
        return ""
    if isinstance(value, str):
        value = parser.parse(value)
    return value.strftime(format_str)


@formatter('formatmoney')
def format_money(value):
    return '$%0.2f' % value


@formatter('formattwodecimals')
def format_2_decimals(value):
    return '%0.2f' % value


@formatter('urlencode')
def urlencode_filter(s):
    if type(s) == 'Markup':
        s = s.unescape()
    s = s.encode('utf8')
    s = urllib.parse.quote(s)
    return Markup(s)


def register_formatters(app):
    for k, f in formatters.items():
        logger.debug("Registering formatter: %s as function %s.%s", k, f.__module__, f.__name__)
        app.jinja_env.filters[k] = f

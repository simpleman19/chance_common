from flask import g, abort
from chance_common.common.db.sqlalchemy.models.user import User
from functools import wraps
from chance_common.common.custom_logging import get_logger

logger = get_logger(__name__)


def load_user_from_g(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if g.current_user_guid:
            g.user = User.query.filter_by(guid=g.current_user_guid).one_or_none()
        return func(*args, **kwargs)

    return wrapper


def load_account_from_g(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        load_account()
        return func(*args, **kwargs)

    return wrapper


def load_account_from_g_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        load_account()
        if not g.get('current_user_guid', None) or not g.get('account', None):
            abort(401)
        return func(*args, **kwargs)

    return wrapper


def load_account():
    logger.debug('Load account called')
    if g.get('current_user_guid', None) and not g.get('account', None):
        logger.debug('Loading account')
        g.account = User.query.filter_by(guid=g.current_user_guid).one_or_none()

import glob
import os
import uuid
import time
import string
import random

from bson import ObjectId
from flask import request
from typing import List, Optional
from functools import wraps

from .api_response import ApiException
from .custom_logging import get_logger

logger = get_logger(__name__)


def timestamp():
    """Return the current timestamp as an integer."""
    return int(time.time())


def guid():
    return str(uuid.uuid4()).replace('-', '')


def get_files_in_directory(directory: str, extensions: Optional[List[str]] = None):
    if extensions is None:
        extensions = ["mp4"]
    absolute_path = os.path.abspath(directory)

    files = []
    if os.path.isdir(absolute_path):
        for ext in extensions:
            files.extend(glob.glob(absolute_path + '/*.' + ext, recursive=False))
    return files


def require_json(required_attrs: List[str] = None):
    """
    Decorator used to require json and passes the parsed json into the function as the json kwarg
    :param required_attrs: json dict items that are required
    :return: wrapped function
    """
    if required_attrs is None:
        required_attrs = []

    def require_json_decorator(func):
        @wraps(func)
        def require_json_func_wrapper(*args, **kwargs):
            logger.debug("Checking if json was sent on request")
            if request.json:
                logger.debug("Found json in request")
                content = request.json
                for attr in required_attrs:
                    if attr not in content:
                        logger.warn("%s.%s called missing: %s in request", func.__module__, func.__name__, attr)
                        raise ApiException(status=400, message="Missing: " + attr + " in json request")
                return func(*args, data=content, **kwargs)
            else:
                logger.info("%s.%s called without json in request", func.__module__, func.__name__)
                raise ApiException(message="Not a json request", status=400)

        return require_json_func_wrapper

    return require_json_decorator


def rand_string_not_cryptographic(length: int = 8):
    alphabet = string.ascii_lowercase + string.digits + string.ascii_uppercase
    return ''.join(random.choices(alphabet, k=length))


def get_from_config_or_environ(key: str, app):
    val = None
    if app:
        val = app.config.get(key, None)
    if not val:
        val = os.environ.get(key)
    return val


class ToDictMixin:
    excluded_fields: List

    def to_dict(self, include_props=True):
        ret_dict = {}
        excluded_fields = getattr(self, 'excluded_fields', [])
        fields = filter(lambda a: not a.startswith('__') and a not in excluded_fields, dir(self))
        if not include_props:
            fields = filter(lambda a: not isinstance(getattr(type(self), a, None), property), fields)
        for f in fields:
            val = getattr(self, f, None)
            if val:
                if isinstance(val, str) or isinstance(val, int):
                    ret_dict[f] = val
                if isinstance(val, ObjectId):
                    ret_dict[f] = str(val)
        return ret_dict


class FromDictMixin:
    excluded_fields: List

    @classmethod
    def from_dict(cls, source_dict: dict):
        obj = cls()
        fields = filter(lambda a: not a.startswith('__') and a, source_dict.keys())
        for f in fields:
            val = source_dict.get(f, None)
            if val:
                setattr(obj, f, val)
        return obj

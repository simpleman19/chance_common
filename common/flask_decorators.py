import inspect
from functools import wraps
from types import GeneratorType
from typing import Dict

from flask import render_template, Blueprint, Response, make_response

from chance_common.common.custom_logging import get_logger
from chance_common.common.api_response import ApiResult
from chance_common.common.forms import CommonFlaskForm
from chance_common.common.utils import ToDictMixin

logger = get_logger(__name__)


class EndpointData(object):
    def __init__(self, blueprint: Blueprint, base_endpoint: str, template: str, endpoint_name: str, view_func, options):
        self.blueprint: Blueprint = blueprint
        self.base_endpoint: str = base_endpoint
        self.template: str = template
        self.endpoint_name = endpoint_name
        self.view_func = view_func
        self.options = options


templated_endpoints: Dict[str, EndpointData] = {}
api_endpoints: Dict[str, EndpointData] = {}


def __handle_api_func(f, *args, **kwargs):
    f_args, _, _, _, _, _, _ = inspect.getfullargspec(f)
    if 'is_api' in f_args:
        kwargs['is_api'] = True
    ctx = f(*args, **kwargs)
    if ctx is None:
        ctx = {}
    elif not isinstance(ctx, dict) and not isinstance(ctx, list):
        return ctx
    cookies = None
    if isinstance(ctx, dict):
        cookies = ctx.pop('cookies', None)
    resp: Response = ApiResult(value=convert_to_json(ctx)).to_response()
    if cookies:
        for k, v in cookies.items():
            resp.set_cookie(k, value=v.get('value', ''), expires=v.get('expires'))
    return resp


def __handle_templated_func(f, template, *args, **kwargs):
    f_args, _, _, _, _, _, _ = inspect.getfullargspec(f)
    if 'is_api' in f_args:
        kwargs['is_api'] = True
    ctx = f(*args, **kwargs)
    if ctx is None:
        ctx = {}
    elif not isinstance(ctx, dict):
        return ctx
    cookies = None
    if isinstance(ctx, dict):
        cookies = ctx.pop('cookies', None)
    resp: Response = make_response(render_template(template, **ctx))
    if cookies:
        for k, v in cookies.items():
            resp.set_cookie(k, value=v.get('value', ''), expires=v.get('expires'))
    return resp


def register_endpoints():
    logger.info("Registering endpoints...")
    for k, e in templated_endpoints.items():
        logger.debug(f"Registering templated endpoint: {k} at {e.base_endpoint}")
        if e.view_func:
            e.blueprint.add_url_rule(rule=e.base_endpoint, endpoint=e.endpoint_name, view_func=e.view_func, **e.options)
    for k, e in api_endpoints.items():
        name = e.endpoint_name + '_api'
        url = '/api' + e.base_endpoint
        logger.debug(f"Registering api endpoint: {name} at {url}")
        if e.view_func:
            e.blueprint.add_url_rule(rule=url, endpoint=name, view_func=e.view_func, **e.options)


def templated_or_api(blueprint: Blueprint, base_endpoint: str, template, **options):
    def decorator(f):
        key = f.__name__
        logger.debug(f"Adding endpoint {f.__module__}.{f.__name__} to registered templated and api endpoints")

        @wraps(f)
        def decorated_template_function(*args, **kwargs):
            return __handle_templated_func(f, template, *args, **kwargs)

        data = EndpointData(blueprint, base_endpoint, template, key, decorated_template_function, options)
        templated_endpoints[key] = data

        @wraps(f)
        def decorated_api_function(*args, **kwargs):
            return __handle_api_func(f, *args, **kwargs)

        data = EndpointData(blueprint, base_endpoint, template, key, decorated_api_function, options)
        api_endpoints[key] = data

        return decorated_template_function

    return decorator


def api(blueprint: Blueprint, base_endpoint: str, **options):
    def decorator(f):
        key = f.__name__
        logger.debug(f"Adding endpoint {f.__module__}.{f.__name__} to registered api endpoints")

        @wraps(f)
        def decorated_api_function(*args, **kwargs):
            return __handle_api_func(f, *args, **kwargs)

        data = EndpointData(blueprint, base_endpoint, '', key, decorated_api_function, options)
        api_endpoints[key] = data

        return decorated_api_function

    return decorator


def templated(blueprint: Blueprint, base_endpoint: str, template, **options):
    def decorator(f):
        key = f.__name__
        logger.debug(f"Adding endpoint {f.__module__}.{f.__name__} to registered templated endpoints")

        @wraps(f)
        def decorated_template_function(*args, **kwargs):
            return __handle_templated_func(f, template, *args, **kwargs)

        data = EndpointData(blueprint, base_endpoint, template, key, decorated_template_function, options)
        templated_endpoints[key] = data

        return decorated_template_function

    return decorator


def convert_to_json(obj):
    if isinstance(obj, dict):
        return {k: convert_to_json(v) for k, v in obj.items()}
    elif isinstance(obj, list) or isinstance(obj, GeneratorType):
        return [convert_to_json(o) for o in obj]
    elif isinstance(obj, ToDictMixin) or isinstance(obj, CommonFlaskForm):
        return obj.to_dict()
    else:
        return obj

import os
from typing import Optional
from chance_common.common import requests
from flask import request

from chance_common.common.custom_logging import get_logger

logger = get_logger(__name__)

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0'
DEFAULT_HEADERS = {'User-Agent': USER_AGENT}


def get_default_headers_with_auth() -> dict:
    headers = DEFAULT_HEADERS
    if request:
        auth = request.headers.get("Authorization", None)
        headers['Authorization'] = auth
    return headers


def get_url_and_return_json(url, valid_status_codes=None, headers=None, json=None) -> Optional[dict]:
    if not valid_status_codes:
        valid_status_codes = [200, 201]
    if not headers:
        headers = DEFAULT_HEADERS
    resp: requests.Response = requests.get(url, headers=headers, json=json)
    logger.debug("Got response back from url: %s response: %s", url, resp)
    if resp.status_code in valid_status_codes and resp.json():
        logger.debug("Got json back from url: %s response: %s", url, resp.json())
        return resp.json()
    else:
        logger.error("Failed to get content from url: %s and response: %s", url, resp)
        return None

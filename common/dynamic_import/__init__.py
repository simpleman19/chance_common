import os
import importlib

from chance_common.common.custom_logging import get_logger

logger = get_logger(__name__)


def import_module_from_filename(base_module_path, dir_path, filename):
    # lets import the module
    base_package = dir_path.replace(base_module_path + os.sep, '').replace(os.sep, '.')
    filename_no_ext, _ = os.path.splitext(filename)
    module_path = base_package + '.' + filename_no_ext.replace(os.sep, '.')
    logger.debug(f"Dynamically importing module: {module_path}")
    return importlib.import_module(module_path)

import os
from pathlib import Path

from chance_common.common.custom_logging import get_logger
from chance_common.common.dynamic_import import import_module_from_filename

logger = get_logger(__name__)


def scan_models(base_path, model_dirs, excludes):
    logger.debug(f"Scanning for sql alchemy models in: {base_path}")
    for dirpath, dirnames, filenames in os.walk(base_path):
        head, tail = os.path.split(dirpath)
        if tail in model_dirs:
            # there should be models
            for filename in filenames:
                if filename.endswith('.py') and \
                        filename not in excludes:
                    new_base = str(Path(base_path).parent)
                    import_module_from_filename(new_base, dirpath, filename)

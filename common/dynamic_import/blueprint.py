from flask import Blueprint
from pathlib import Path
import os
from chance_common.common.custom_logging import get_logger
from chance_common.common.dynamic_import import import_module_from_filename

logger = get_logger(__name__)


def scan_blueprints(root_path, blueprint_folder='routes'):
    logger.debug(f"Scanning for blueprints in: {root_path}")
    blueprints = []
    for dirpath, dirnames, filenames in os.walk(os.path.join(root_path, blueprint_folder)):
        for filename in filenames:
            if filename.endswith('.py'):
                new_base = str(Path(root_path).parent)
                imported_module = import_module_from_filename(new_base, dirpath, filename)

                for i in dir(imported_module):
                    attribute = getattr(imported_module, i)

                    if isinstance(attribute, Blueprint):
                        blueprints.append(attribute)
    return blueprints

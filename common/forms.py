from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.compat import iteritems


def hydrate(form: FlaskForm, obj: object):
    for name, field, in iteritems(form._fields):
        if obj is not None and hasattr(obj, name):
            obj.__setattr__(name, field.data)
    return obj


class CommonFlaskForm(FlaskForm):
    def to_dict(self):
        ret_dict = {}
        for name, field, in iteritems(self._fields):
            validators = []
            for v in field.validators:
                field_flags = []
                for f in v.field_flags:
                    field_flags.append(f)
                validators.append({
                    'field_flags': field_flags
                })
            if field.type == 'CSRFTokenField':
                data = field.current_token
            else:
                data = field.data
            ret_dict[name] = {
                'data': data,
                'type': field.type,
                'validators': validators,
            }
        return ret_dict


class MoneyField(StringField):
    pass

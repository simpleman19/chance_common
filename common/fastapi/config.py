from chance_common.common.pydantic.config import Config, load_config, config_class


class FastApiSettings(Config):
    APP_NAME: str = "TODO SET NAME"
    URL_PREFIX = '/auth'
    MONGO_URI: str = "mongodb://root:password@localhost:27017/test?authSource=admin"
    SQLALCHEMY_DATABASE_URI: str = 'sqlite:///database/app.db'

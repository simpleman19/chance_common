import os

from chance_common.common.custom_logging import get_logger

logger = get_logger(__name__)

cwd = os.getcwd()  # Fallback


class Config(object):
    def __init__(self, *args, **kwargs):
        if not hasattr(self, 'BASE_PATH'):
            self.BASE_PATH = kwargs.get('basedir', cwd)
        if not hasattr(self, 'ROOT_PATH'):
            self.ROOT_PATH = self.BASE_PATH
        self.DEBUG = False
        self.TESTING = False
        self.SECRET_KEY = os.environ.get('SECRET_KEY',
                                         '51f52814-0071-11e6-a247-000ec6c2372c')
        self.JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY', self.SECRET_KEY)
        self.SQLITE_PATH = os.path.join(self.BASE_PATH, 'database', 'app.db')
        self.SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + self.SQLITE_PATH
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False
        self.VALID_EXTENSIONS = [".mp3"]
        self.TOKEN_EXPIRE_MINUTES = 60
        self.TOKEN_EXPIRE_MINUTES_REMEMBER_ME = 60 * 24 * 14  # 3 days for remember me
        self.TEMPLATE_DIRS = [os.path.join(self.BASE_PATH, 'templates')]
        self.MODEL_DIRS = [
            'models',
        ]
        self.MODEL_EXCLUDE_FILES = [
            '__init__.py',
        ]
        self.JSONIFY_MIMETYPE = 'application/json'

    def get(self, name, default=None):
        return getattr(self, name, default)


class DevelopmentConfig(Config):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.DEBUG = True
        self.SESSION_COOKIE_SECURE = False
        self.TEMPLATES_AUTO_RELOAD = True


class ProductionConfig(Config):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class TestingConfig(ProductionConfig):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.SQLITE_PATH = os.path.join(self.BASE_PATH, 'test', 'app.db')
        self.SQLALCHEMY_DATABASE_URI = 'sqlite:///' + self.SQLITE_PATH


class UiTestingConfig(ProductionConfig):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.WEB_DRIVER_PATH = os.environ.get('WEB_DRIVER_PATH', '/home/simpleman19/Documents/selenium/chromedriver')
        self.BASE_URL = os.environ.get('BASE_URL', 'http://localhost:5000')
        self.SQLITE_PATH = os.path.join(self.BASE_PATH, 'test', 'app.db')
        self.SQLALCHEMY_DATABASE_URI = 'sqlite:///' + self.SQLITE_PATH
        self.START_FLASK = True
        self.WIPE_DATABASE = True


class UiProductionTestingConfig(ProductionConfig):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.WEB_DRIVER_PATH = os.environ.get('WEB_DRIVER_PATH', '/home/simpleman19/Documents/selenium/chromedriver')
        self.BASE_URL = os.environ.get('BASE_URL', 'http://localhost:5500')
        self.START_FLASK = False
        self.WIPE_DATABASE = False


def load_config(config_name=None):
    if not config_name:
        config_name = os.environ.get('APP_CONFIG', 'development')
    logger.info("Starting up in: " + config_name + " config")
    return config_map[config_name]()


def config_class(config_name):
    def inner(cls):
        config_map[config_name] = cls
        return cls

    return inner


config_map = {
    'development': DevelopmentConfig,
}

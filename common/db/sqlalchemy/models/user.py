from . import db, BaseModel
from .auth import PasswordEntry


class User(BaseModel):
    __tablename__ = 'user'
    owner_guid = db.Column(db.String(32), nullable=False, default='none', index=True)

    current_password_id = db.Column(db.Integer, db.ForeignKey('user_password.id'))
    current_password = db.relationship("PasswordEntry")

    email = db.Column(db.String(250), nullable=False, index=True)
    first_name = db.Column(db.String(100), nullable=False, default='')
    last_name = db.Column(db.String(100), nullable=False, default='')

    user_level = db.Column(db.Integer, nullable=False, default=100)

    __table_args__ = (db.UniqueConstraint('owner_guid', 'email', name='_unique_owner_and_email'),)

    excluded_fields = ['password']

    def __init__(self, email: str, password: PasswordEntry, first: str = '', last: str = '', owner_guid: str = 'none'):
        super().__init__()
        self.owner_guid = owner_guid
        self.email = email
        self.current_password = password
        self.first_name = first
        self.last_name = last

    def __str__(self):
        return f'Account Guid: {self.guid}'

    def __repr__(self):
        return f'<Account({self.guid})?'

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __ne__(self, other):
        """Define a non-equality test"""
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented

from . import db, BaseModel
import bcrypt


class PasswordEntry(BaseModel):
    __tablename__ = 'user_password'
    password = db.Column(db.LargeBinary(100), nullable=False)
    valid = db.Column(db.Boolean(), nullable=False, default=True)
    excluded_fields = ['password']

    def verify_password(self, password):
        if password:
            return bcrypt.checkpw(password.encode('utf-8'), self.password)
        else:
            return False

    def set_password(self, new_password, old_password=""):
        if self.password is None or self.password == "":
            self.password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
            return True
        elif bcrypt.checkpw(old_password.encode('utf-8'), self.password):
            self.password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
            return False

    def __init__(self, password=""):
        super().__init__()
        if not password == "":
            self.set_password(password)

    def __str__(self):
        return f'User Guid: {self.guid} id: {str(self.id)}'

    def __repr__(self):
        return f'<User({self.guid})?'

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __ne__(self, other):
        """Define a non-equality test"""
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented

from datetime import datetime
from typing import List

from flask_sqlalchemy import SQLAlchemy
from chance_common.common.utils import guid, ToDictMixin

db = SQLAlchemy()


class BaseModel(db.Model, ToDictMixin):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    guid = db.Column(db.String(32), nullable=False, unique=True)
    deleted = db.Column(db.Boolean, nullable=False, default=False)
    excluded_fields = []

    def __init__(self):
        self.guid = guid()

from flask import abort
from pymongo import collection
from pymongo import database
from pymongo import mongo_client

from chance_common.common.db.mongo.document import BaseDocument


class MongoClient(mongo_client.MongoClient):
    """Wrapper for :class:`~pymongo.mongo_client.MongoClient`.

    Returns instances of Flask-PyMongo
    :class:`~flask_pymongo.wrappers.Database` instead of native PyMongo
    :class:`~pymongo.database.Database` when accessed with dot notation.

    """

    def __getattr__(self, name):  # noqa: D105
        attr = super(MongoClient, self).__getattr__(name)
        if isinstance(attr, database.Database):
            return Database(self, name)
        return attr

    def __getitem__(self, item):  # noqa: D105
        attr = super(MongoClient, self).__getitem__(item)
        if isinstance(attr, database.Database):
            return Database(self, item)
        return attr


class Database(database.Database):
    """Wrapper for :class:`~pymongo.database.Database`.

    Returns instances of Flask-PyMongo
    :class:`~flask_pymongo.wrappers.Collection` instead of native PyMongo
    :class:`~pymongo.collection.Collection` when accessed with dot notation.

    """

    def __getattr__(self, name):  # noqa: D105
        attr = super(Database, self).__getattr__(name)
        if isinstance(attr, collection.Collection):
            return Collection(self, name)
        return attr

    def __getitem__(self, item):  # noqa: D105
        item_ = super(Database, self).__getitem__(item)
        if isinstance(item_, collection.Collection):
            return Collection(self, item)
        return item_


class Collection(collection.Collection):
    """Sub-class of PyMongo :class:`~pymongo.collection.Collection` with helpers.

    """

    def __getattr__(self, name):  # noqa: D105
        attr = super(Collection, self).__getattr__(name)
        if isinstance(attr, collection.Collection):
            db = self._Collection__database
            return Collection(db, attr.name)
        return attr

    def __getitem__(self, item):  # noqa: D105
        item_ = super(Collection, self).__getitem__(item)
        if isinstance(item_, collection.Collection):
            db = self._Collection__database
            return Collection(db, item_.name)
        return item_

    def find_one_or_404(self, *args, **kwargs):
        """Find a single document or raise a 404.

        This is like :meth:`~pymongo.collection.Collection.find_one`, but
        rather than returning ``None``, cause a 404 Not Found HTTP status
        on the request.

        .. code-block:: python

            @app.route("/user/<username>")
            def user_profile(username):
                user = mongo.db.users.find_one_or_404({"_id": username})
                return render_template("user.html",
                    user=user)

        """
        found = self.find_one(*args, **kwargs)
        if found is None:
            abort(404)
        return found

    def insert_one(self, document, bypass_document_validation=False, session=None):
        if isinstance(document, BaseDocument):
            document = document.to_dict(include_props=False)
        return super().insert_one(document, bypass_document_validation, session)

    def insert_many(self, documents, ordered=True, bypass_document_validation=False, session=None):
        return super().insert_many(documents, ordered, bypass_document_validation, session)

    def find_one(self, filter=None, *args, **kwargs):
        ret_val = super().find_one(filter, *args, **kwargs)
        return ret_val

    def find(self, *args, **kwargs):
        ret_val = super().find(*args, **kwargs)
        for val in ret_val:
            if val.get('_type', None) == 'CustomBaseDocument':
                yield BaseDocument.from_dict(val)
            else:
                yield val

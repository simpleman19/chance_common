__all__ = ("PyMongo", "ASCENDING", "DESCENDING")

from functools import partial
from pymongo import uri_parser
import pymongo

from .helpers import BSONObjectIdConverter, JSONEncoder
from .wrappers import MongoClient

text_type = str
num_type = int

DESCENDING = pymongo.DESCENDING
"""Descending sort order."""

ASCENDING = pymongo.ASCENDING
"""Ascending sort order."""


class PyMongo(object):
    """Manages MongoDB connections for your Flask app.

    PyMongo objects provide access to the MongoDB server via the :attr:`db`
    and :attr:`cx` attributes. You must either pass the :class:`~flask.Flask`
    app to the constructor, or call :meth:`init_app`.

    PyMongo accepts a MongoDB URI via the ``MONGO_URI`` Flask configuration
    variable, or as an argument to the constructor or ``init_app``. See
    :meth:`init_app` for more detail.

    """

    initialized = False

    def __init__(self, app=None, uri=None, json_options=None, *args, **kwargs):
        self.cx = None
        self.db = None
        self._json_encoder = partial(JSONEncoder, json_options=json_options)

        if app is not None:
            self.init_app(app, uri, *args, **kwargs)

    def init_app(self, app, uri=None, *args, **kwargs):
        if self.initialized:
            return
        if uri is None:
            uri = app.config.get("MONGO_URI", None)
        if uri is not None:
            args = tuple([uri] + list(args))
        else:
            raise ValueError(
                "You must specify a URI or set the MONGO_URI Flask config variable",
            )

        parsed_uri = uri_parser.parse_uri(uri)
        database_name = parsed_uri["database"]

        # Try to delay connecting, in case the app is loaded before forking, per
        # http://api.mongodb.com/python/current/faq.html#is-pymongo-fork-safe
        kwargs.setdefault("connect", False)

        self.cx = MongoClient(*args, **kwargs)
        if database_name:
            self.db = self.cx[database_name]

        app.url_map.converters["ObjectId"] = BSONObjectIdConverter
        app.json_encoder = self._json_encoder
        self.initialized = True

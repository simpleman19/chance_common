from datetime import datetime

from bson.objectid import ObjectId

from chance_common.common.utils import ToDictMixin, FromDictMixin


class BaseDocument(ToDictMixin, FromDictMixin):
    _id: ObjectId
    creation_timestamp: datetime
    _type: str
    excluded_fields = []

    def __init__(self, *args, **kwargs):
        self.creation_timestamp = datetime.utcnow()
        self._type = 'CustomBaseDocument'

import os
from functools import lru_cache
from pydantic import BaseSettings

from chance_common.common.custom_logging import get_logger

logger = get_logger(__name__)


class Config(BaseSettings):

    def get(self, name, default=None):
        return getattr(self, name, default)

    class Config:
        case_sensitive = False


@lru_cache()
def load_config(config_name=None):
    if not config_name:
        config_name = os.environ.get('APP_CONFIG', 'development')
    logger.info("Loading config: " + config_name + " config")
    return config_map[config_name]()


def config_class(config_name):
    def inner(cls):
        config_map[config_name] = cls
        return cls

    return inner


config_map = {}
